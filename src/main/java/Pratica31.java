
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Andreas
 */
public class Pratica31 {

    private static Date inicio;
    private static String meuNome;
    private static GregorianCalendar dataNascimento;
    private static Date fim;

    public static void main(String[] args) {
        inicio = new Date();
        meuNome = "Andreas Anael Pereira Gomes";
        System.out.println(meuNome.toUpperCase());

        meuNome = meuNome.substring(22, 23).toUpperCase() + meuNome.substring(23).toLowerCase() + ", " + meuNome.substring(0, 1).toUpperCase() + ". " + meuNome.substring(8, 9).toUpperCase() + ". " + meuNome.substring(14, 15).toUpperCase() + ".";
        System.out.println(meuNome);

        dataNascimento = new GregorianCalendar(1992, 11, 25);
        System.out.println(((new Date()).getTime() - dataNascimento.getTime().getTime()) / (1000 * 24 * 60 * 60));

        fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());

    }
}
